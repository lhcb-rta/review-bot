"""Label MRs according to the rules."""
import logging

import click

from . import clogging
from .definitions import RTA_BRANCHES, RTA_PROJECTS
from .helpers import get_gitlab_server

log = logging.getLogger(__name__)


def main():
    """Run main business logic."""
    LABEL = "RTA"

    gl = get_gitlab_server()
    projects = [gl.projects.get(p) for p in RTA_PROJECTS]

    all_mrs = []
    for project in projects:
        for branch in RTA_BRANCHES:
            extra_args = {"not[labels]": LABEL}
            mrs = project.mergerequests.list(
                target_branch=branch, state="opened", iterator=True, **extra_args
            )
            all_mrs.extend(mrs)

    log.info(f"Found {len(all_mrs)} MRs to label in total.")

    for mr in all_mrs:
        log.info(f"Adding label: {LABEL} to {mr.references['full']}: {mr.web_url}")
        mr.labels.append(LABEL)
        mr.save()


@click.command()
@clogging.option()
def cli(*args, **kwargs):
    """Label MRs according to the rules."""
    main(*args, **kwargs)  # pragma: no cover
