"""Configuration parameters."""

GITLAB_URL = "https://gitlab.cern.ch/"

RTA_PROJECTS = [
    "lhcb/LHCb",
    "lhcb/Lbcom",
    "lhcb/Rec",
    "lhcb/Phys",
    "lhcb/Allen",
    "lhcb/Moore",
    "lhcb/Alignment",
    "lhcb/MooreOnline",
    "lhcb/MooreAnalysis",
    "lhcb/AlignmentOnline",
    "lhcb/Brunel",
]

RTA_BRANCHES = [
    "master",
    "2024-patches",
]
