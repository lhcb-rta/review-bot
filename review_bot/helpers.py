"""Helper functions to interact with Gitlab."""
import logging
import os

import gitlab

from .definitions import GITLAB_URL

log = logging.getLogger(__name__)


def get_gitlab_server(token_path=".gitlab.token", token_var="GITLAB_TOKEN"):
    """Connect to a Gitlab server taking care of tokens and defaults."""
    try:
        with open(token_path) as f:
            token = f.read().strip()
    except FileNotFoundError:
        log.debug(f"Token not found at {os.path.realpath(token_path)}")
        token = os.getenv(token_var)
        if token is None:
            log.debug(f"Token not found in environment variable {token_var}")
    return gitlab.Gitlab(
        GITLAB_URL,
        private_token=token,
        per_page=100,
    )
