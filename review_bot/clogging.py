"""Logging setup for Click applications."""
import logging
import textwrap

import click

_root_logger = logging.getLogger("")
_root_logger.setLevel(logging.INFO)


class _ClickFormatter(logging.Formatter):
    """Colourful logging formatter."""

    def __init__(self):
        def formatter(color=None):
            return logging.Formatter(
                click.style("%(levelname)-8s", fg=color, bold=True) + " %(message)s"
            )

        self._default = formatter()
        self._warning = formatter("yellow")
        self._error = formatter("red")
        self._header_len = 9

    def format(self, record):
        if record.levelno < logging.WARNING:
            formatter = self._default
        elif record.levelno < logging.ERROR:
            formatter = self._warning
        else:
            formatter = self._error
        record.msg = textwrap.indent(record.msg, " " * self._header_len).lstrip()
        return formatter.format(record)


class _ClickEchoHandler(logging.Handler):
    def emit(self, record):
        try:
            msg = self.format(record)
            click.echo(msg, err=True)
        except Exception:
            self.handleError(record)


def _setup_logging(level):
    console = _ClickEchoHandler()
    console.setFormatter(_ClickFormatter())
    _root_logger.addHandler(console)
    _root_logger.setLevel(level)


def option(f=None):
    """Add options to control logging verbosity."""
    if f is None:
        return option

    def callback(ctx, param, level):
        if level is None:
            level = logging.INFO
        _setup_logging(level=level)
        return None

    f = click.option(
        "--debug",
        "_log_level",
        flag_value=logging.DEBUG,
        expose_value=False,
        callback=callback,
    )(f)
    f = click.option(
        "--quiet",
        "_log_level",
        flag_value=logging.WARNING,
        expose_value=False,
    )(f)
    return f
