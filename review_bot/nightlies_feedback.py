"""Post feedback from nightly builds to MRs."""
import logging

import click

from . import clogging

log = logging.getLogger(__name__)


def main():
    """Run the main business logic."""
    log.warning("Hello")


@click.command()
@clogging.option()
def cli(*args, **kwargs):
    """CLI entrypoint."""
    main(*args, **kwargs)
