"""Scripts to automate the MR review process."""
import importlib.metadata

__version__ = importlib.metadata.version(__name__)
