# Review bot

Automate the MR review for the LHCb physics software: post feedback from nightlies, etc.

The automatic actions are implemented as either pipeline schedules or pipeliene triggers.
See more details in [docs/deploy.md].

## Actions

- `label_mrs`: Label MRs according to the following rules. Runs every 10 min.
    1. Add an `RTA` label to all projects for which RTA is responsible.

## Developing

Install [Poetry](https://python-poetry.org/docs/) or use the one comming
with LbEnv. Then clone the project and set up the environment:

```console
poetry config --local virtualenvs.in-project true  # optional, helps with VSCode
poetry install  # creates .venv
poetry run pre-commit install
```

Then modify, test and commit as usual.

```console
poetry run pytest
```

### Adding and modifying tests

We use [VCR.py](https://vcrpy.readthedocs.io/en/latest/index.html) and
[pytest-recording](https://github.com/kiwicom/pytest-recording)
to simplify and speed up tests that make HTTP requests by recording
and replaying them.
In practice, it is enough to decorate the tests where you need this
with `@pytest.mark.vcr`.

Some GitLab API calls will need to be authenticated (even for public projects).
It is strongly advised to [create](https://gitlab.cern.ch/-/profile/personal_access_tokens)
a dedicated __read_api__ personal access token and save it in the file `.gitlab.token`.
In this way, you can't accidentally make write/modify requests and also minimise the
consequences in case the token leaks.
Secrets that are part of requests must be filtered out so that they are not committed.
This is done for all tests with [tests/conftest.py] but can also be extended per test.

When adding a new test or updating an existing test, a _cassette_ needs
to be created or updated. If you just run `pytest`, you'll see an error such as

```
vcr.errors.CannotOverwriteExistingCassetteException: Can't overwrite existing cassette ('~/review-bot/tests/cassettes/test_label_mrs/test_label_mrs.yaml') in your current record mode ('none').
No match for the request (<Request (GET) https://gitlab.cern.ch/api/v4/projects/lhcb%2FLHCb>) was found.
No similar requests, that have not been played, found.
```

This is because pytest-recording by default does not allow network requests
(the record mode is `none`).
Run the following manually only for the test(s) that you want to add/update
and a new/updated casseette will be produced.

```console
rm ~/review-bot/tests/cassettes/test_label_mrs/test_label_mrs.yaml
poetry run pytest --record-mode=once tests/test_label_mrs.py
```

Before running the above command think about ensuring appropriate test conditions.
For example, in the `label_mrs` case, make sure there is at least one MR to be
labeled (e.g. by removing the `RTA` label from an open MR).

## Running

```console
poetry run label_mrs
```

## Credits

Package structure inspired from [https://github.com/pronovic/apologies].
