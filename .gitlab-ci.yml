default:
  image: registry.cern.ch/docker.io/library/python:3.9
  before_script:
    - python --version
    - bash scripts/install_poetry.sh
    - export PATH=$PWD/.poetry/bin:$PATH
    - poetry config --local virtualenvs.in-project true
    - poetry install

.cached_venv:
  cache: &default_cache
    - key:
        prefix: poetry
        files:
          - scripts/install_poetry.sh
      paths:
        - .poetry
    - key:
        prefix: venv
        files:
          - poetry.lock
      paths:
        - .venv

.cached_venv_pull:
  cache:
    - key:
        prefix: poetry
        files:
          - scripts/install_poetry.sh
      paths:
        - .poetry
      policy: pull
    - key:
        prefix: venv
        files:
          - poetry.lock
      paths:
        - .venv
      policy: pull

### CI jobs

.ci_job:
  extends: .cached_venv
  rules:
    # start CI pipelines on push
    - if: '$CI_PIPELINE_SOURCE == "push"'
    # but never for other sources (such as schedules and triggers)
    - when: never

pre_commit:
  extends: .ci_job
  variables:
    PRE_COMMIT_HOME: ${CI_PROJECT_DIR}/.cache/pre-commit
  script:
    - git fetch origin ${CI_DEFAULT_BRANCH}
    - poetry run pre-commit run --from-ref FETCH_HEAD --to-ref HEAD
  cache:
    - *default_cache  # merge with the default cache instead of overwriting it
    - key: ${CI_JOB_NAME}
      paths:
        - ${PRE_COMMIT_HOME}

test:
  extends: .ci_job
  script:
    - poetry run pytest
  coverage: '/^TOTAL.*\s+(\d+\%)$/'

### Worker jobs

.worker_job:
  extends: .cached_venv_pull
  rules:
    # WORKER_JOB_NAME is a variable that need to be passed to the trigger
    # or added to the pipeline schedule. It selects which of the worker jobs
    # to run.
    - if: '($CI_PIPELINE_SOURCE == "schedule" || $CI_PIPELINE_SOURCE == "trigger" || $CI_PIPELINE_SOURCE == "web") && $CI_JOB_NAME == $WORKER_JOB_NAME'
    - when: never
  # ensure the job is mutually exclusive across different pipelines
  resource_group: ${CI_JOB_NAME}
  artifacts:
    expire_in: 1 week

label_mrs:
  extends: .worker_job
  # ensure jobs don't pile up (timeout < schedule interval)
  timeout: 10 minutes
  script:
    - export GITLAB_TOKEN=$lhcbsoft_gitlab_token
    - poetry run label_mrs --debug
