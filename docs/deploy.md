# Deploy instructions

- Add the bot user as a maintainer of this repo.
- Login as the bot user and create a new token with API access.
- Add a variable to https://gitlab.cern.ch/lhcb-rta/review-bot/-/settings/ci_cd
    - key: `lhcbsoft_gitlab_token`
    - value: `<the bot token>`
    - flags: protected and masked
- For jobs that are triggered by some event, create a trigger token owned by the bot user. For example:
    ```
    GITLAB_TOKEN=<the bot token> poetry run python docs/update_hook.py trigger <some_job>
    ```
    where `<some_job>` is the trigger description which is used to identify it.
- For jobs that are triggered on a schedule, create a pipeline schedule. For example:
    ```
    GITLAB_TOKEN=<the bot token> poetry run python docs/update_hook.py schedule label_mrs --var WORKER_JOB_NAME=label_mrs
    ```
