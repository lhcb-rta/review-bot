set -euxo pipefail

POETRY_VERSION=1.1.12

if [ ! -e .poetry/bin/poetry ] ; then
    # Poetry is not installed (note: $HOME tells get-poetry.py were to install)
    curl -sSLO https://raw.githubusercontent.com/python-poetry/poetry/${POETRY_VERSION}/get-poetry.py
	HOME=$PWD python get-poetry.py --version ${POETRY_VERSION}
	rm get-poetry.py
fi
