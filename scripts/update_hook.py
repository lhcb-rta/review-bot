"""Create or update a pipeline trigger or schedule."""

import os

import click
import gitlab

from review_bot import clogging


@click.group()
@clogging.option()
@click.pass_context
def cli(ctx):
    """Create/update a pipeline trigger or schedule."""
    gl = gitlab.Gitlab(
        "https://gitlab.cern.ch", private_token=os.environ["GITLAB_TOKEN"]
    )
    gl.auth()
    ctx.ensure_object(dict)
    ctx.obj["user"] = gl.user
    ctx.obj["project"] = gl.projects.get("lhcb-rta/review-bot")


@cli.command()
@click.argument("description")
@click.pass_context
def trigger(ctx, description):
    """Create/update a trigger hook called DESCRIPTION."""
    current_user = ctx.obj["user"]
    project = ctx.obj["project"]
    for trigger in project.triggers.list(all=True):
        if trigger.description == description:
            if trigger.owner["id"] != current_user.id:
                exit(
                    "Trigger exists but owner is different: "
                    + f"{trigger.owner['username']}"
                )
            break
    else:
        trigger = project.triggers.create(data=dict(description=description))

    trigger_cmd = f"""curl -X POST \\
        -F token={trigger.token} \\
        -F ref={project.default_branch} \\
        https://gitlab.cern.ch/api/v4/projects/{project.id}/trigger/pipeline
    """
    click.echo(trigger_cmd)


@cli.command()
@click.argument("description")
@click.option(
    "--ref", default="master", help="The branch or tag name that is triggered."
)
@click.option("--cron", default="*/10 * * * *", help="The cron schedule.")
@click.option("--var", multiple=True, help="Variables to set, e.g. --var key=value")
@click.pass_context
def schedule(ctx, description, ref, cron, var):
    """Create/update a pipeline schedule called DESCRIPTION."""
    vars = [v.split("=", 1) for v in var]
    project = ctx.obj["project"]
    for schedule in project.pipelineschedules.list(all=True):
        if schedule.description == description:
            # Recreate schedule in case there are differences.
            # We must do it this way since there is no way to list the
            # variables, see
            # https://gitlab.com/gitlab-org/gitlab/-/issues/250850
            schedule.delete()
    schedule = project.pipelineschedules.create(
        data=dict(description=description, ref=ref, cron=cron, active=False)
    )
    for key, value in vars:
        schedule.variables.create(dict(key=key, value=value))
    click.echo("New schedule created, activate it manually.")
    click.echo(f"    {project.web_url}/-/pipeline_schedules/{schedule.id}/edit")


if __name__ == "__main__":
    cli()
