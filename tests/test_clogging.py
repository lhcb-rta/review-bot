import logging
import re

import click
from click.testing import CliRunner

from review_bot import clogging


@click.command()
@clogging.option()
def dummy_cli():
    log = logging.getLogger(__name__)
    log.info("info")
    log.warning("warning")
    log.error("error")


def test_clogging():
    runner = CliRunner()
    result = runner.invoke(dummy_cli, [])
    assert result.exit_code == 0
    assert re.match(
        r"INFO.*info.*WARNING.*warning.*ERROR.*error", result.output, flags=re.DOTALL
    )
