from unittest.mock import patch

import pytest

from review_bot import label_mrs


@pytest.mark.vcr
@patch("gitlab.mixins.SaveMixin.save")
def test_label_mrs(mock):
    label_mrs.main()
    mock.assert_called_with()
