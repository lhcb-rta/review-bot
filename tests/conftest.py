"""Global fixtures for the entire directory."""
from typing import Iterator
from unittest.mock import DEFAULT, patch

import pytest


@pytest.fixture(scope="session", autouse=True)
def assert_no_gitlab_actions() -> Iterator[None]:
    """Ensure that write requests are never called in tests."""
    with patch.multiple(
        "gitlab.Gitlab", http_post=DEFAULT, http_put=DEFAULT, http_delete=DEFAULT
    ) as values:
        yield
        called = sum([v.call_args_list for v in values.values() if v.called], [])
        assert not any(called), "Some write requests were made, use patch!"


@pytest.fixture(scope="session", autouse=True)
def vcr_config():
    """Filter out secrets."""
    return {
        "filter_headers": [
            "PRIVATE-TOKEN",  # GitLab token
        ]
    }
